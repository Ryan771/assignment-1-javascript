/*
Great work here, Ryan.  Your app functions as required
and follows all the business rules.  Everything is named
correctly and you've got the comments I needed to see.
Nicely work adding in the 'not required' Regex validation
20/20
*/

//Create function to load provinces
function loadProvinces(){
  var provArray = ["Alberta", "British Columbia", "Ontario", "Nova Scotia",
                    "Saskatchewan", "Manitoba", "Newfoundland", "Quebec",
                    "New Brunswick", "Prince Edward Island"]

  var opt = "<option value=''>-Select-</option>"
//Load selectbox
  for(i=0;i<provArray.length;i++){
    opt += "<option value='" + provArray[i] + "'>" + provArray[i] + "</option>"
  }
  document.getElementById('cboProv').innerHTML=opt;
}

//create validate function
function validateForm(){
          var emailFormat = /(\w+\.)*\w+@([a-zA-Z]+\.)+[a-zA-Z]{2,6}/;
          var select = document.getElementById('cboProv').value
          var name = document.getElementById('txtName').value;
          var email = document.getElementById('txtEmail').value;

          if(select==""){
              alert('Please select a province');
              document.getElementById('cboProv').focus();
              return false;
          }

          if(name==""){
              alert('Please enter a name');
              document.getElementById('txtName').focus();
              return false;
          }

          if(email==""){
              alert('Please enter an email address');
              document.getElementById("txtEmail").focus();
              return false;
          } else {
            if (!emailFormat.test(email)){
              alert("Please enter a valid email format");
              document.getElementById('txtEmail').focus();
              return false;
            }
          }

          alert('Successfully submitted');
      }
