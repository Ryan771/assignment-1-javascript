// Well done, Ryan.  Your game looks and works fantastic
// You've added in validation, statistics, and some
// nice GUI features.  You've also got the comments and
// function names I needed to see.
// 20/20

//create variables

var gamePieces = ["rock", "paper", "scissors", "dynamite"];

var playerScore=0;

var computerScore=0;

var playerChoiceSpan;

var computerChoiceSpan;

//function to load array
function promptGame() {

    var options
    while (true) {
        var playerChoice = prompt("Please choose one of these options: " + gamePieces).toLowerCase();

        if(!playerChoice || /^(rock|paper|scissors|dynamite)$/.test(playerChoice)){
            options = ("rock|paper|scissors|dynamite".split("|").indexOf(playerChoice)+1);
            break;
            }else{
                alert("Please enter a valid options");
            }
    }

    var rndComputerChoice = gamePieces[getRandomGamePiece(gamePieces.length)];
    var computerChoice = rndComputerChoice;


    displayImage();

    whoWins(playerChoice,computerChoice);

    function getRandomGamePiece(arrayLength){
        var rnd = Math.floor((Math.random() * arrayLength) + 0);
        return rnd;
    }

    function displayImage(){
        $('#firstImage').css("display","block");
        $('#secondImage').css("display","block");
        $('#playerLabel').css("display", "block");
        $('#compLabel').css("display", "block");

        if(playerChoice=="rock"){
            $('#firstImage').attr('src',"http://www.pvhc.net/img224/qvawqiqnrrqkthxlfmgq.png");
        }else if (playerChoice=="paper"){
            $('#firstImage').attr('src',"http://www.pvhc.net/img224/xgqhkgxygufazcjpcxay.png");
        }else if (playerChoice=="scissors"){
            $('#firstImage').attr('src',"http://www.pvhc.net/img224/hgfermpycqpqktpowlpf.png");
        }else if (playerChoice=="dynamite"){
            $('#firstImage').attr('src',"https://png.icons8.com/color/1600/dynamite.png");
        }

        if(computerChoice=="rock"){
            $('#secondImage').attr('src',"http://www.pvhc.net/img224/qvawqiqnrrqkthxlfmgq.png");
        }else if (computerChoice=="paper"){
            $('#secondImage').attr('src',"http://www.pvhc.net/img224/xgqhkgxygufazcjpcxay.png");
        }else if (computerChoice=="scissors"){
            $('#secondImage').attr('src',"http://www.pvhc.net/img224/hgfermpycqpqktpowlpf.png");
        }else if (computerChoice=="dynamite"){
            $('#secondImage').attr('src',"https://png.icons8.com/color/1600/dynamite.png");
        }
    }
}


//function to compare playerChoice and computerChoice
function whoWins(playerChoice,computerChoice) {

    switch (playerChoice + computerChoice){

        case "rockrock":
        case "scissorscissors":
        case "dynamitedynamite":
        case "paperpaper":
            $('#results').html("<h2>Tie! Try again!</h2>");
            break;

        case "rockpaper":
        case "rockdynamite":
        case "paperdynamite":
        case "paperscissors":
        case "scissorsrock":
        case "dynamitescissors":
            playerChoiceSpan=playerChoice
            computerChoiceSpan=computerChoice;
            lose();
            break;

        case "rockscissors":
        case "paperrock":
        case "scissorsdynamite":
        case "scissorspaper":
        case "dynamiterock":
        case "dynamitepaper":
            playerChoiceSpan=playerChoice
            computerChoiceSpan=computerChoice;
            win();
            break;
        }
}

    function win(){
            playerScore++;
            $('#results').html("<h2>" + playerChoiceSpan + " beats " + computerChoiceSpan + "! You win!</h2>");
            $('#playerScore').html("<h2> Player-"+  playerScore + " "  +"Computer-"+computerScore + "</h2>");
    }

    function lose(){
            computerScore++;
            $('#results').html("<h2>" + computerChoiceSpan + " beats " + playerChoiceSpan +"! You lost, Try Again!<h2>");
            $('#playerScore').html("<h2>Player-"+  playerScore + " " + "Computer-"+computerScore + "</h2>");
    }
